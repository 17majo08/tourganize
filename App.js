import React from 'react';

import Providers from './src/navigation';
import {
	NavigationContainer,
	DarkTheme as NavigationDarkTheme,
	DefaultTheme as NavigationDefaultTheme,
} from '@react-navigation/native';
import {
	Provider as PaperProvider,
	DarkTheme as PaperDarkTheme,
	DefaultTheme as PaperDefaultTheme,
} from 'react-native-paper';

import {
	AppRegistry,
	KeyboardAvoidingView,
	TouchableWithoutFeedback,
	Keyboard,
} from 'react-native';
import { name as appName } from './app.json';

import { ThemeContext } from './src/theme/ThemeContext';

import FirebaseAdapter from './src/firebase';

import {
	REACT_APP_FIREBASE_API_KEY,
	REACT_APP_AUTH_DOMAIN,
	REACT_APP_PROJECT_ID,
	REACT_APP_STORAGE_BUCKET,
	REACT_APP_MESSAGING_SENDER_ID,
	REACT_APP_APP_ID,
	REACT_APP_MEASUREMENT_ID,
} from './src/environment/Environment';

console.log(REACT_APP_FIREBASE_API_KEY);

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
FirebaseAdapter.Initialize({
	apiKey: REACT_APP_FIREBASE_API_KEY,
	authDomain: REACT_APP_AUTH_DOMAIN,
	projectId: REACT_APP_PROJECT_ID,
	storageBucket: REACT_APP_STORAGE_BUCKET,
	messagingSenderId: REACT_APP_MESSAGING_SENDER_ID,
	appId: REACT_APP_APP_ID,
	measurementId: REACT_APP_MEASUREMENT_ID,
});

// Fixes ugly warnings
import { LogBox } from 'react-native';
import _ from 'lodash';

LogBox.ignoreLogs(['Warning:...']); // ignore specific logs
LogBox.ignoreAllLogs(); // ignore all logs
const _console = _.clone(console);
console.warn = (message) => {
	if (message.indexOf('Setting a timer') <= -1) {
		_console.warn(message);
	}
};
// Fixes ugly warnings

const CustomTheme = {
	dark: true,
	mode: 'adaptive',
	roundness: 4,
	colors: {
		primary: '#DA0037',
		background: '#171717',
		text: '#EDEDED',
		surface: '#2A3952', // This controls react-native-paper-cards + appbar + statusbar :)
		card: '#2A3952EB', // This controls react-navigation cards + appbar + statusbar :)
		accent: '#DA0037', // This controls the FAB
		placeholder: '#999999', // Placeholder for text inputs etc

		backdrop: '#000000', // Not sure what this one does
		onSurface: '#000000', // Not sure what this one does
		notification: '#000000', // Not sure what this one does
		disabled: '#000000', // Not sure what this one does
		border: '#000000', // React-natigation specific. Not sure what this one does
	},
	fonts: {
		light: {
			fontFamily: 'sans-serif-light',
			fontWeight: 'normal',
		},
		medium: {
			fontFamily: 'sans-serif-medium',
			fontWeight: 'normal',
		},
		regular: {
			fontFamily: 'sans-serif',
			fontWeight: 'normal',
		},
		thin: {
			fontFamily: 'sans-serif-thin',
			fontWeight: 'normal',
		},
	},
	animation: {
		scale: 1,
	},
};

export default function App() {
	return (
		<KeyboardAvoidingView
			behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
			style={{ flex: 1 }}>
			<TouchableWithoutFeedback onPress={Keyboard.dismiss}>
				<PaperProvider theme={CustomTheme}>
					<NavigationContainer theme={CustomTheme}>
						<ThemeContext.Provider value={CustomTheme}>
							<Providers />
						</ThemeContext.Provider>
					</NavigationContainer>
				</PaperProvider>
			</TouchableWithoutFeedback>
		</KeyboardAvoidingView>
	);
}

AppRegistry.registerComponent(appName, () => App);
