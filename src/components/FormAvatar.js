import React from 'react';
import { TouchableOpacity } from 'react-native';
import { Avatar } from 'react-native-paper';

import LaunchImagePicker from '../helpers/ImagePicker';

export default function FormAvatar(props) {
	return (
		<TouchableOpacity
			disabled={!props.isClickable}
			style={props.style}
			onPress={() => LaunchImagePicker(props.onImageSelected, [1, 1])}>
			{props.src === '' && <Avatar.Icon size={props.size} icon='account' />}
			{props.src !== '' && (
				<Avatar.Image size={props.size} source={{ uri: props.src }} />
			)}
		</TouchableOpacity>
	);
}
