import React, { useContext } from 'react';
import { StyleSheet } from 'react-native';
import { Modal, Portal, Button, Provider } from 'react-native-paper';

import PropTypes from 'prop-types';

import { ThemeContext } from '../theme/ThemeContext';

export default function FormModal(props) {
	const theme = useContext(ThemeContext);

	return (
		<Provider theme={theme}>
			<Portal>
				<Modal
					visible={props.isOpen}
					onDismiss={props.onDismiss}
					contentContainerStyle={styles.container}>
					{props.children}
					<Button disabled={!props.canAccept} onPress={props.onAccept}>
						Add
					</Button>
					<Button onPress={props.onDismiss}>Cancel</Button>
				</Modal>
			</Portal>
		</Provider>
	);
}

FormModal.propTypes = {
	isOpen: PropTypes.bool,
	canAccept: PropTypes.bool,
	onAccept: PropTypes.func,
	onDismiss: PropTypes.func,
	children: PropTypes.element.isRequired,
};

FormModal.defaultProps = {
	canAccept: true,
	onAccept: () => alert('Implement onAccept :)'),
	onDismiss: () => alert('Implement onDismiss :)'),
};

const styles = StyleSheet.create({
	container: {
		padding: 20,
		justifyContent: 'center',
		alignItems: 'center',
		marginHorizontal: 20,
	},
});
