import React from 'react';
import { TouchableOpacity } from 'react-native';
import { List, Divider } from 'react-native-paper';

import ScreenName from '../screens/ScreenNames';

export default function GroupListItem(props) {
	return (
		<>
			<TouchableOpacity
				onPress={() =>
					props.navigation.navigate(ScreenName.Group, {
						groupId: props.group.id,
					})
				}>
				<List.Item
					title={props.group.name}
					description={props.group.name}
					right={() => <List.Icon icon='chevron-right' />}
				/>
			</TouchableOpacity>
			<Divider />
		</>
	);
}
