import React from 'react';
import { View } from 'react-native';
import { List, Divider } from 'react-native-paper';

export default function PaymentListSection(props) {
	return (
		<>
			{props.payments.map((x, index) => (
				<View key={index}>
					<List.Item
						key={index}
						title={x.description}
						description={`${x.amount} ${x.currency}`}
						left={(props) => <List.Icon {...props} icon='currency-eur' />}
					/>
					<Divider />
				</View>
			))}
		</>
	);
}
