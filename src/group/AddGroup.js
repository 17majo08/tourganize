import React, { useState } from 'react';
import { Title } from 'react-native-paper';

import FormInput from '../components/FormInput';
import FormModal from '../components/FormModal';

import FirebaseAdapter from '../firebase';

export default function AddGroup(props) {
	const [groupName, setGroupName] = useState('');

	const closeModal = () => {
		setGroupName('');
		props.onDismiss();
	};

	return (
		<FormModal
			isOpen={props.isOpen}
			canAccept={groupName !== ''}
			onAccept={() => {
				FirebaseAdapter.AddGroup(groupName);
				closeModal();
			}}
			onDismiss={closeModal}>
			<>
				<Title>Add new travel group!</Title>
				<FormInput
					labelName='Group name'
					value={groupName}
					autoCapitalize='none'
					onChangeText={(name) => setGroupName(name)}
				/>
			</>
		</FormModal>
	);
}
