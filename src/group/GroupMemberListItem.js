import React from 'react';
import { List } from 'react-native-paper';

import FormAvatar from '../components/FormAvatar';

export default function GroupMemberListItem(props) {
	return (
		<List.Item
			title={props.user.displayName}
			description='Insert some catchy caption? :)'
			left={() => (
				<FormAvatar isClickable={false} size={56} src={props.profileImage} />
			)}
		/>
	);
}
