import React, { useState, useContext } from 'react';
import { Appbar, Menu } from 'react-native-paper';

import pickImage from '../helpers/ImagePicker';

import { GroupContext } from './GroupContext';

import FirebaseAdapter from '../firebase';

import ScreenName from '../screens/ScreenNames';

export default function GroupAppbar(props) {
	const [menuVisible, setMenuVisible] = useState(false);

	const group = useContext(GroupContext);

	return (
		<Appbar.Header>
			<Appbar.BackAction onPress={props.navigation.goBack} />
			<Appbar.Content
				title={group.name}
				onPress={() =>
					props.navigation.navigate(ScreenName.GroupMember, {
						group: group,
					})
				}
			/>
			<Menu
				visible={menuVisible}
				onDismiss={() => setMenuVisible(false)}
				anchor={
					<Appbar.Action
						icon='dots-vertical'
						color='white'
						onPress={() => setMenuVisible(true)}
					/>
				}>
				<Menu.Item
					onPress={() => {
						setMenuVisible(false);
						props.navigation.navigate(ScreenName.AddMemory, {
							groupId: group.id,
						});
					}}
					title='Share a story'
				/>
				<Menu.Item
					onPress={() => {
						setMenuVisible(false);
						props.navigation.navigate(ScreenName.AddPayment, {
							groupId: group.id,
						});
					}}
					title='Add booking'
				/>
				<Menu.Item
					onPress={() => {
						setMenuVisible(false);
						pickImage(
							(uri) => FirebaseAdapter.UploadGroupImage(uri, group.id),
							undefined
						);
					}}
					title='Upload image'
				/>
			</Menu>
		</Appbar.Header>
	);
}
