import React, { useState, useEffect } from 'react';
import { TouchableOpacity } from 'react-native';

import { createNativeStackNavigator } from '@react-navigation/native-stack';

import { Ionicons } from '@expo/vector-icons';

import LoginScreen from '../screens/stack/authorization/LoginScreen';
import SignupScreen from '../screens/stack/authorization/SignupScreen';

import GroupListScreen from '../screens/stack/home/GroupListScreen';
import SettingsScreen from '../screens/stack/home/SettingsScreen';
import GroupScreen from '../screens/stack/group/GroupScreen';
import GroupMemberScreen from '../screens/stack/group/GroupMemberScreen';

import AddPaymentScreen from '../screens/stack/group/AddPaymentScreen';
import AddStoryScreen from '../screens/stack/group/AddStoryScreen';

import ScreenName from '../screens/ScreenNames';

import FirebaseAdapter from '../firebase';

const Stack = createNativeStackNavigator();

export default function index() {
	[isSignedIn, setSignedIn] = useState(false);
	[isOptionsOpen, setOptionsOpen] = useState(false);

	useEffect(() => {
		FirebaseAdapter.Attach(setSignedIn);

		return () => FirebaseAdapter.Detach(setSignedIn);
	}, []);

	return (
		<Stack.Navigator>
			{isSignedIn ? (
				<>
					<Stack.Screen
						name={ScreenName.Home}
						component={GroupListScreen}
						options={({ navigation }) => ({
							headerRight: () => (
								<TouchableOpacity
									onPress={() => navigation.navigate(ScreenName.Settings)}>
									<Ionicons name='settings-outline' size={24} color='#EDEDED' />
								</TouchableOpacity>
							),
						})}
					/>
					<Stack.Screen name={ScreenName.Settings} component={SettingsScreen} />
					<Stack.Screen
						name={ScreenName.Group}
						component={GroupScreen}
						options={{
							headerShown: false,
						}}
					/>
					<Stack.Screen
						name={ScreenName.GroupMember}
						component={GroupMemberScreen}
					/>
					<Stack.Screen
						name={ScreenName.AddPayment}
						component={AddPaymentScreen}
					/>
					<Stack.Screen
						name={ScreenName.AddMemory}
						component={AddStoryScreen}
					/>
				</>
			) : (
				<>
					<Stack.Screen name={ScreenName.Login} component={LoginScreen} />
					<Stack.Screen name={ScreenName.SignUp} component={SignupScreen} />
				</>
			)}
		</Stack.Navigator>
	);
}
