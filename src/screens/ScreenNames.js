export default class ScreenName {
	// Stack
	static Login = 'Log in';
	static SignUp = 'Sign up';
	static Home = 'Tourganize';
	static Settings = 'Settings';
	static Group = 'Group';
	static GroupMember = 'Members';

	// Tabs
	static GroupDiary = 'Stories';
	static GroupPayments = 'Expenses';
	static GroupPictures = 'Memories';
	static GroupTemp = 'Temporary';

	// Stack adds
	static AddPayment = 'Add booking';
	static AddMemory = 'Add story';
	static AddPicture = 'Add picture';
}
