import React, { useState, useEffect } from 'react';
import { StyleSheet, SafeAreaView, ScrollView } from 'react-native';
import { List, FAB } from 'react-native-paper';

import AddGroupModal from '../../../group/AddGroup';
import GroupListItem from '../../../group/GroupListItem';

import FirebaseAdapter from '../../../firebase';

export default function GroupListScreen({ navigation }) {
	[isModalOpen, setModalOpen] = useState(false);
	[groups, setGroups] = useState([]);

	useEffect(() => {
		const subscriber = FirebaseAdapter.SubscribeToGroups((groups) => {
			setGroups(groups);
		});

		return () => subscriber();
	}, []);

	return (
		<SafeAreaView style={styles.container}>
			<ScrollView contentContainerStyle={styles.scrollView}>
				<List.Section style={styles.list}>
					<List.Subheader>Groups</List.Subheader>
					{groups.map((group, index) => (
						<GroupListItem key={index} group={group} navigation={navigation} />
					))}
				</List.Section>
				<AddGroupModal
					isOpen={isModalOpen}
					onDismiss={() => setModalOpen(false)}
				/>
				<FAB
					style={styles.fab}
					icon='plus'
					color='black'
					// theme={FabTheme}
					onPress={() => setModalOpen(true)}
					disabled={isModalOpen}
				/>
			</ScrollView>
		</SafeAreaView>
	);
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
	},
	scrollView: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
	},
	fab: {
		position: 'absolute',
		margin: 32,
		right: 0,
		bottom: 0,
	},
	list: {
		alignSelf: 'stretch',
	},
});
