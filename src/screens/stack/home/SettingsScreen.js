import React from 'react';
import { StyleSheet, SafeAreaView, ScrollView } from 'react-native';
import { List } from 'react-native-paper';

import FirebaseAdapter from '../../../firebase';

export default function SettingsScreen() {
	return (
		<SafeAreaView style={styles.container}>
			<ScrollView contentContainerStyle={styles.scrollView}>
				<List.Section>
					<List.Subheader>Other</List.Subheader>
					<List.Item
						title='Sign out'
						left={() => <List.Icon icon='power-standby' />}
						onPress={() => FirebaseAdapter.SignOut()}
					/>
				</List.Section>
			</ScrollView>
		</SafeAreaView>
	);
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
	},
	scrollView: {
		flex: 1,
	},
});
