import React, { useState } from 'react';
import { View, StyleSheet, SafeAreaView, ScrollView } from 'react-native';
import { Title, Subheading, Button, Menu, TextInput } from 'react-native-paper';

import FormInput from '../../../components/FormInput';
import FormButton from '../../../components/FormButton';

import FirebaseAdapter from '../../../firebase';

export default function AddPaymentScreen({ navigation, route }) {
	const [isCurrencyModalOpen, setCurrencyMenuOpen] = useState(false);
	const [isTypeMenuOpen, setTypeMenuOpen] = useState(false);

	const [description, setDescription] = useState('');
	const [amount, setAmount] = useState('');

	const [currency, setCurrency] = useState('EUR');
	const [type, setType] = useState('Living');

	const onCurrencyChange = (c) => {
		setCurrency(c);
		setCurrencyMenuOpen(false);
	};

	const onTypeChange = (c) => {
		setType(c);
		setTypeMenuOpen(false);
	};

	const isValidForm = () => {
		return description !== '' && amount !== '';
	};

	return (
		<SafeAreaView style={styles.container}>
			<ScrollView contentContainerStyle={styles.scrollView}>
				<Title style={styles.title}>Add booking details</Title>
				<FormInput
					labelName='Description'
					value={description}
					autoCapitalize='sentences'
					onChangeText={(x) => setDescription(x)}
				/>
				<FormInput
					labelName='Amount'
					value={amount}
					autoCapitalize='none'
					keyboardType='numeric'
					onChangeText={(x) => setAmount(x)}
					right={<TextInput.Affix text={currency} />}
				/>
				<View
					style={{
						flexDirection: 'row',
						justifyContent: 'center',
					}}>
					<Menu
						visible={isCurrencyModalOpen}
						onDismiss={() => setCurrencyMenuOpen(false)}
						anchor={
							<Button
								icon='currency-eur'
								mode='text'
								onPress={() => setCurrencyMenuOpen(true)}>
								Change currency
							</Button>
						}>
						<Menu.Item onPress={() => onCurrencyChange('EUR')} title='EUR' />
						<Menu.Item onPress={() => onCurrencyChange('CHF')} title='CHF' />
						<Menu.Item onPress={() => onCurrencyChange('SEK')} title='SEK' />
					</Menu>
				</View>

				<Subheading style={styles.subTitle}>Type: {type}</Subheading>
				<View
					style={{
						flexDirection: 'row',
						justifyContent: 'center',
					}}>
					<Menu
						visible={isTypeMenuOpen}
						onDismiss={() => setTypeMenuOpen(false)}
						anchor={
							<Button
								icon='airplane'
								mode='text'
								onPress={() => setTypeMenuOpen(true)}>
								Change type of booking
							</Button>
						}>
						<Menu.Item onPress={() => onTypeChange('Living')} title='Living' />
						<Menu.Item
							onPress={() => onTypeChange('Transportation')}
							title='Transportation'
						/>
						<Menu.Item onPress={() => onTypeChange('Food')} title='Food' />
						<Menu.Item onPress={() => onTypeChange('Other')} title='Other' />
					</Menu>
				</View>

				<FormButton
					title='Add booking'
					modeValue='contained'
					disabled={!isValidForm()}
					onPress={() => {
						FirebaseAdapter.AddPayment(
							route.params.groupId,
							description,
							parseFloat(amount),
							currency,
							type
						);
						navigation.goBack();
					}}
				/>
			</ScrollView>
		</SafeAreaView>
	);
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
	},
	scrollView: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center',
	},
	currencyContainer: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center',
	},
	title: {
		paddingBottom: 10,
	},
	subTitle: {
		paddingTop: 10,
	},
});
