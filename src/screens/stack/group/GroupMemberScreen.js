import React from 'react';
import { StyleSheet, SafeAreaView, ScrollView } from 'react-native';
import { List } from 'react-native-paper';

import GroupMemberListItem from '../../../group/GroupMemberListItem';

export default function GroupMemberScreen({ navigation, route }) {
	const users = Object.entries(route.params.group.members);

	return (
		<SafeAreaView style={styles.container}>
			<ScrollView contentContainerStyle={styles.scrollView}>
				<List.Section>
					<List.Subheader>Members of {route.params.group.name}</List.Subheader>
					{users.map(([userId, user], index) => (
						<GroupMemberListItem
							key={index}
							user={user}
							profileImage={route.params.group.profilePictures[userId]}
						/>
					))}
				</List.Section>
			</ScrollView>
		</SafeAreaView>
	);
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
	},
	scrollView: {
		flex: 1,
	},
});
