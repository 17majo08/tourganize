import React, { useState, useLayoutEffect } from 'react';
import {
	View,
	TextInput,
	StyleSheet,
	SafeAreaView,
	ScrollView,
} from 'react-native';
import { Divider, Appbar } from 'react-native-paper';

import FirebaseAdapter from '../../../firebase';

export default function AddStoryScreen({ navigation, route }) {
	const [title, setTitle] = useState('');
	const [story, setStory] = useState('');

	useLayoutEffect(() => {
		navigation.setOptions({
			headerRight: () => (
				<Appbar.Action
					icon='plus-box-multiple'
					color='#EDEDED'
					disabled={title === '' || story === ''}
					onPress={() => {
						FirebaseAdapter.AddStory(route.params.groupId, title, story);
						navigation.goBack();
					}}
				/>
			),
		});
	}, [navigation, title, story, route.params.groupId]);

	return (
		<SafeAreaView style={styles.container}>
			<ScrollView style={styles.scrollView}>
				<View style={styles.inputContainer}>
					<TextInput
						style={styles.title}
						autoCapitalize='sentences'
						placeholder='Title'
						placeholderTextColor='#999999'
						onChangeText={(text) => setTitle(text)}
						value={title}
					/>
					<Divider />
					<TextInput
						multiline
						onChangeText={(text) => setStory(text)}
						value={story}
						style={styles.story}
						autoCapitalize='sentences'
						placeholder='Compose your story!'
						placeholderTextColor='#999999'
					/>
				</View>
			</ScrollView>
		</SafeAreaView>
	);
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
	},
	scrollView: {
		flex: 1,
	},
	inputContainer: {
		flex: 1,
		margin: 10,
	},
	title: {
		padding: 15,
		fontSize: 24,
		color: '#EDEDED',
	},
	story: {
		flex: 1,
		padding: 10,
		fontSize: 18,
		color: '#EDEDED',
	},
});
