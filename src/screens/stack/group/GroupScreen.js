import React, { useState, useEffect } from 'react';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';

import { Ionicons } from '@expo/vector-icons';

import DiaryTabScreen from '../../tabs/DiaryTabScreen';
import PaymentsTabScreen from '../../tabs/PaymentsTabScreen';
import PicturesTabScreen from '../../tabs/PicturesTabScreen';

import GroupAppbar from '../../../group/GroupAppbar';

import FirebaseAdapter from '../../../firebase';

import ScreenName from '../../ScreenNames';

import { GroupContext } from '../../../group/GroupContext';

const Tab = createMaterialTopTabNavigator();

export default function GroupScreen({ route, navigation }) {
	const [group, setGroup] = useState({
		name: '',
		owner: '',
		members: [],
		images: [],
		bookings: [],
		stories: [],
	});
	const [members, setMembers] = useState([]);
	const [memberPictures, setMemberPictures] = useState({});

	useEffect(() => {
		const subscriber = FirebaseAdapter.SubscribeToGroup(
			route.params.groupId,
			(g) => setGroup(g)
		);

		return () => subscriber();
	}, [route.params.groupId]);

	useEffect(() => {
		if (group.members.length === 0) {
			return;
		}

		const subscriber = FirebaseAdapter.SubscribeToMembers(
			group.members,
			(members) => {
				setMembers(members);
			}
		);
		return () => subscriber();
	}, [group.members]);

	useEffect(() => {
		if (members.length === 0) {
			return;
		}

		FirebaseAdapter.FetchProfileImages(Object.values(members), (urls) =>
			setMemberPictures(urls)
		);
	}, [members]);

	return (
		<GroupContext.Provider
			value={{
				...group,
				members: members,
				profilePictures: memberPictures,
			}}>
			<GroupAppbar navigation={navigation} />
			{members.length !== 0 && (
				<Tab.Navigator
					tabBarPosition='bottom'
					initialRouteName={ScreenName.GroupDiary}
					backBehavior='none'
					screenOptions={({ route }) => ({
						tabBarActiveTintColor: '#DA0037',
						tabBarInactiveTintColor: '#999999',
					})}>
					<Tab.Screen
						name={ScreenName.GroupPayments}
						component={PaymentsTabScreen}
						options={PaymentsTabOptions}
					/>
					<Tab.Screen
						name={ScreenName.GroupDiary}
						component={DiaryTabScreen}
						options={DiaryTabOptions}
					/>
					<Tab.Screen
						name={ScreenName.GroupPictures}
						component={PicturesTabScreen}
						options={PicturesTabOptions}
					/>
				</Tab.Navigator>
			)}
		</GroupContext.Provider>
	);
}

const PaymentsTabOptions = {
	tabBarShowLabel: false,
	tabBarIcon: ({ focused, color, size }) => (
		<Ionicons
			name={focused ? 'card' : 'card-outline'}
			size={24}
			color={color}
		/>
	),
};

const DiaryTabOptions = {
	tabBarShowLabel: false,
	tabBarIcon: ({ focused, color, size }) => (
		<Ionicons
			name={focused ? 'document-text' : 'document-text-outline'}
			size={24}
			color={color}
		/>
	),
};

const PicturesTabOptions = {
	tabBarShowLabel: false,
	tabBarIcon: ({ focused, color, size }) => (
		<Ionicons
			name={focused ? 'image' : 'image-outline'}
			size={24}
			color={color}
		/>
	),
};
