import React, { useState } from 'react';
import { StyleSheet, Alert, SafeAreaView, ScrollView } from 'react-native';
import { Title } from 'react-native-paper';

import FormButton from '../../../components/FormButton';
import FormInput from '../../../components/FormInput';

import ScreenName from '../../ScreenNames';

import FirebaseAdapter from '../../../firebase';

export default function LoginScreen({ navigation }) {
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');

	return (
		<SafeAreaView style={styles.container}>
			<ScrollView contentContainerStyle={styles.scrollView}>
				<Title style={styles.titleText}>Welcome!</Title>
				<FormInput
					labelName='Email'
					value={email}
					autoCapitalize='none'
					keyboardType='email-address'
					onChangeText={(userEmail) => setEmail(userEmail)}
				/>
				<FormInput
					labelName='Password'
					value={password}
					secureTextEntry={true}
					autoCapitalize='none'
					onChangeText={(userPassword) => setPassword(userPassword)}
				/>
				<FormButton
					title='Login'
					modeValue='contained'
					labelStyle={styles.loginButtonLabel}
					onPress={() =>
						FirebaseAdapter.Login(email, password, (error) =>
							Alert.alert('Ooops!', error.message)
						)
					}
					disabled={email === '' || password === ''}
				/>
				<FormButton
					title='Sign up here'
					modeValue='text'
					uppercase={false}
					labelStyle={styles.navButtonText}
					onPress={() => navigation.navigate(ScreenName.SignUp)}
				/>
			</ScrollView>
		</SafeAreaView>
	);
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
	},
	scrollView: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
	},
	titleText: {
		fontSize: 24,
		marginBottom: 10,
	},
	loginButtonLabel: {
		fontSize: 22,
	},
	navButtonText: {
		fontSize: 16,
	},
});
