import React, { useState } from 'react';
import { StyleSheet, Alert, SafeAreaView, ScrollView } from 'react-native';
import { IconButton, Title } from 'react-native-paper';

import FormButton from '../../../components/FormButton';
import FormInput from '../../../components/FormInput';
import FormAvatar from '../../../components/FormAvatar';

import FirebaseAdapter from '../../../firebase';

export default function SignupScreen({ navigation }) {
	const [displayName, setDisplayName] = useState('');
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [profileImage, setProfileImage] = useState('');

	return (
		<SafeAreaView style={styles.container}>
			<ScrollView contentContainerStyle={styles.scrollView}>
				<Title style={styles.titleText}>Let's get started!</Title>
				<FormAvatar
					style={styles.avatar}
					src={profileImage}
					size={168}
					isClickable={true}
					onImageSelected={(uri) => setProfileImage(uri)}
				/>
				<FormInput
					labelName='Display Name'
					value={displayName}
					autoCapitalize='sentences'
					onChangeText={(userDisplayName) => setDisplayName(userDisplayName)}
				/>
				<FormInput
					labelName='Email'
					value={email}
					autoCapitalize='none'
					keyboardType='email-address'
					onChangeText={(userEmail) => setEmail(userEmail)}
				/>
				<FormInput
					labelName='Password'
					value={password}
					secureTextEntry={true}
					autoCapitalize='none'
					onChangeText={(userPassword) => setPassword(userPassword)}
				/>
				<FormButton
					title='Signup'
					modeValue='contained'
					labelStyle={styles.loginButtonLabel}
					onPress={() =>
						FirebaseAdapter.SignUp(
							displayName,
							email,
							password,
							profileImage,
							(error) => Alert.alert('Ooops!', error.message)
						)
					}
					disabled={displayName === '' || email === '' || password === ''}
				/>
				<IconButton
					icon='keyboard-backspace'
					size={30}
					style={styles.navButton}
					onPress={() => navigation.goBack()}
					color='#DA0037'
				/>
			</ScrollView>
		</SafeAreaView>
	);
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
	},
	scrollView: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
	},
	titleText: {
		fontSize: 24,
		marginBottom: 10,
	},
	loginButtonLabel: {
		fontSize: 22,
	},
	navButtonText: {
		fontSize: 18,
	},
	navButton: {
		marginTop: 10,
	},
	avatar: {
		margin: 15,
	},
});
