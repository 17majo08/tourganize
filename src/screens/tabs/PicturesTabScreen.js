import React, { useState, useEffect, useContext } from 'react';
import { View } from 'react-native';

import GridImageView from 'react-native-grid-image-viewer';

import { GroupContext } from '../../group/GroupContext';

import FirebaseAdapter from '../../firebase';

export default function PicturesTabScreen() {
	const [urls, setUrls] = useState([]);

	const group = useContext(GroupContext);

	useEffect(() => {
		console.log('fetching images');

		FirebaseAdapter.FetchGroupImages(group.id, (x) => {
			setUrls(x);
		});
	}, [group.images]);

	return (
		<View style={{ flex: 1 }}>
			<GridImageView data={urls} />
		</View>
	);
}
