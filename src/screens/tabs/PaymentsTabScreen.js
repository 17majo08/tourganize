import React, { useState, useEffect, useContext } from 'react';
import { View, StyleSheet, ScrollView, SafeAreaView } from 'react-native';
import { List, Menu, Button } from 'react-native-paper';

import FirebaseAdapter from '../../firebase';

import PaymentListSection from '../../group/PaymentListSection';

import { GroupContext } from '../../group/GroupContext';

import toMainCurrency from '../../helpers/CurrencyConverter';

export default function PaymentsTabScreen() {
	const [grouping, setGrouping] = useState('Type');
	const [isGroupingMenuOpen, setGroupingMenuOpen] = useState(false);

	const [payments, setPayments] = useState([]);
	const [paymentGroups, setPaymentGroups] = useState([]);

	const group = useContext(GroupContext);

	useEffect(() => {
		console.log('fetching payments');

		const subscriber = FirebaseAdapter.SubscribeToPayments(group.id, (x) => {
			setPayments(x);
			setPaymentGroups(regroupPayments(x, grouping));
		});

		return () => subscriber();
	}, [group.bookings]);

	const groupBy = (list, keyGetter) => {
		const map = new Map();
		list.forEach((item) => {
			const key = keyGetter(item);

			const collection = map.get(key);
			if (!collection) {
				map.set(key, [item]);
			} else {
				collection.push(item);
			}
		});

		return map;
	};

	const regroupPayments = (p, g) => {
		switch (g) {
			case 'None':
				return groupBy(p, (_) => 'All');
			case 'Type':
				return groupBy(p, (x) => x.type);
			case 'User':
				return groupBy(p, (x) => group.members[x.userId].displayName);
		}
	};

	const onGroupingChanged = (g) => {
		setGrouping(g);
		setGroupingMenuOpen(false);
		setPaymentGroups(regroupPayments(payments, g));
	};

	return (
		<SafeAreaView style={styles.container}>
			<ScrollView>
				<List.Section>
					<List.Subheader>Bookings</List.Subheader>
					<View
						style={{
							flexDirection: 'row',
						}}>
						<Menu
							visible={isGroupingMenuOpen}
							onDismiss={() => setGroupingMenuOpen(false)}
							anchor={
								<Button mode='text' onPress={() => setGroupingMenuOpen(true)}>
									{`Group by: ${grouping}`}
								</Button>
							}>
							<Menu.Item
								onPress={() => onGroupingChanged('None')}
								title='None'
							/>
							<Menu.Item
								onPress={() => onGroupingChanged('Type')}
								title='Type'
							/>
							<Menu.Item
								onPress={() => onGroupingChanged('User')}
								title='User'
							/>
						</Menu>
					</View>
					{grouping === 'None' && <PaymentListSection payments={payments} />}
					{grouping !== 'None' &&
						Array.from(paymentGroups).map(([type, p], i) => (
							<List.Accordion
								key={i}
								title={type}
								description={`${p
									.reduce(
										(total, x) => total + toMainCurrency(x.amount, x.currency),
										0
									)
									.toFixed(2)} EUR`}>
								<PaymentListSection payments={p} />
							</List.Accordion>
						))}
				</List.Section>
			</ScrollView>
		</SafeAreaView>
	);
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
	},
});
