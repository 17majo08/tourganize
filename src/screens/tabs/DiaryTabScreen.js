import React, { useState, useEffect, useContext } from 'react';
import { View, StyleSheet, SafeAreaView, ScrollView } from 'react-native';
import { Card, Title, Paragraph, Caption } from 'react-native-paper';

import FormAvatar from '../../components/FormAvatar';

import { GroupContext } from '../../group/GroupContext';

import FirebaseAdapter from '../../firebase';

export default function DiaryTabScreen() {
	const [stories, setStories] = useState([]);

	const group = useContext(GroupContext);

	useEffect(() => {
		console.log('fetching stories');

		const subscriber = FirebaseAdapter.SubscribeToStories(group.id, (x) => {
			setStories(x);
		});

		return () => subscriber();
	}, [group.stories]);

	return (
		<SafeAreaView style={styles.container}>
			<ScrollView style={styles.scrollView}>
				{stories.map((story, index) => (
					<Card key={index} style={styles.card}>
						<Card.Content>
							<Title>{story.title}</Title>
							<Paragraph>{story.story}</Paragraph>
							<View
								style={{
									flexDirection: 'row',
									alignItems: 'center',
									justifyContent: 'flex-end',
								}}>
								<FormAvatar
									isClickable={false}
									src={
										group.profilePictures[story.userId] === null
											? ''
											: group.profilePictures[story.userId]
									}
									size={32}
								/>
								<Caption style={{ fontSize: 16, marginLeft: 3 }}>
									{group.members[story.userId].displayName}
								</Caption>
							</View>
						</Card.Content>
					</Card>
				))}
			</ScrollView>
		</SafeAreaView>
	);
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
	},
	scrollView: {
		flex: 1,
	},
	card: {
		margin: 10,
		backgroundColor: '#17171770',
	},
});
