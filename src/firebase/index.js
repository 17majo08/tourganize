import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';
import 'firebase/storage';

import 'react-native-get-random-values';
import { v4 as uuidv4 } from 'uuid';

class Observable {
	observers = [];

	Attach(observer) {
		this.observers.push(observer);
	}

	Detach(observer) {
		this.observers.filter((x) => x !== observer);
	}

	Notify(value) {
		console.log('Notify observers!');

		this.observers.forEach((observer) => observer(value));
	}
}

class FirebaseAdapter extends Observable {
	Initialize(config) {
		console.log('Preparing initialize');

		if (!firebase.apps.length) {
			console.log('Initializing...');

			firebase.initializeApp(config);

			firebase.auth().onAuthStateChanged((user) => {
				if (user) {
					this.Notify(true);
				} else {
					this.Notify(false);
				}
			});
		} else {
			console.log('Already initialized. Using existing.');

			firebase.app();
		}
	}

	Login(email, password, onError) {
		console.log('Loging in...');

		firebase
			.auth()
			.signInWithEmailAndPassword(email, password)
			.then(() => {
				console.log('Logged in with existing account!');
			})
			.catch((error) => onError(error));
	}

	SignUp(username, email, password, imageUri, onError) {
		console.log('Signing up...');

		firebase
			.auth()
			.createUserWithEmailAndPassword(email, password)
			.then((userCredential) => {
				var user = userCredential.user;
				console.log(`Successfully signed up ${user}!`);

				var imagePath = imageUri ? `profiles/${user.uid}.jpeg` : null;

				if (imageUri) {
					this.UploadImage(imageUri, imagePath, user.uid, () =>
						console.log('Uploaded profile image')
					);
				}

				userCredential.user
					.updateProfile({ displayName: username })
					.then((s) => console.log(`Updated profile. ${s}`))
					.catch((error) => onError(error));

				firebase
					.firestore()
					.collection('users')
					.add({
						id: userCredential.user.uid,
						displayName: username,
						photo: imagePath,
					})
					.then((docRef) => {
						console.log('User stored in db.');
					})
					.catch((error) => {
						onError(error);
					});
			})
			.catch((error) => {
				onError(error);
			});
	}

	SignOut() {
		console.log('Signing out...');

		firebase.auth().signOut();
	}

	GetCurrentUser() {
		return firebase.auth().currentUser;
	}

	GetUsername() {
		return this.GetCurrentUser().displayName;
	}

	GetUserId() {
		return this.GetCurrentUser().uid;
	}

	AddGroup(groupName) {
		console.log(`Adding group: ${groupName}`);

		firebase
			.firestore()
			.collection('groups')
			.add({
				name: groupName,
				owner: this.GetUserId(),
				members: [this.GetUserId()],
				images: [],
			})
			.then((docRef) => {
				console.log('Document written with ID: ', docRef.id);
			})
			.catch((error) => {
				console.error('Error adding document: ', error);
			});
	}

	SubscribeToGroup(groupId, callback) {
		return firebase
			.firestore()
			.collection('groups')
			.doc(groupId)
			.onSnapshot((doc) => callback({ ...doc.data(), id: doc.id }));
	}

	SubscribeToGroups(callback) {
		return firebase
			.firestore()
			.collection('groups')
			.where('members', 'array-contains', this.GetUserId())
			.onSnapshot((snapshot) => {
				var groups = [];

				snapshot.forEach((group) =>
					groups.push({ ...group.data(), id: group.id })
				);

				callback(groups);
			});
	}

	SubscribeToMembers(userIds, callback) {
		return firebase
			.firestore()
			.collection('users')
			.where('id', 'in', userIds)
			.onSnapshot((snapshot) => {
				var members = {};

				snapshot.forEach((g) => {
					var member = g.data();

					members[member.id] = member;
				});

				callback(members);
			});
	}

	AddPayment(groupId, description, amount, currency, type) {
		firebase
			.firestore()
			.collection('groups')
			.doc(groupId)

			.collection('bookings')
			.add({
				description: description,
				amount: amount,
				currency: currency,
				type: type,
				userId: this.GetUserId(),
			})
			.then((docRef) => {
				console.log('Added payment with ID: ', docRef.id);
			})
			.catch((error) => {
				console.error('Error adding payment: ', error);
			});
	}

	SubscribeToPayments(groupId, callback) {
		return firebase
			.firestore()
			.collection('groups')
			.doc(groupId)
			.collection('bookings')
			.onSnapshot((snapshot) => {
				var groups = [];

				snapshot.forEach((group) =>
					groups.push({ ...group.data(), id: group.id })
				);

				callback(groups);
			});
	}

	AddStory(groupId, title, story) {
		firebase
			.firestore()
			.collection('groups')
			.doc(groupId)
			.collection('stories')
			.add({
				title: title,
				story: story,
				userId: this.GetUserId(),
			})
			.then((docRef) => {
				console.log('Story added with ID: ', docRef.id);
			})
			.catch((error) => {
				console.error('Error adding story: ', error);
			});
	}

	SubscribeToStories(groupId, callback) {
		return firebase
			.firestore()
			.collection('groups')
			.doc(groupId)
			.collection('stories')
			.onSnapshot((snapshot) => {
				var groups = [];

				snapshot.forEach((group) => groups.push(group.data()));

				callback(groups);
			});
	}

	UploadGroupImage(uri, groupId) {
		var userId = this.GetUserId();
		var path = `groups/${groupId}/${uuidv4()}.jpeg`;

		this.UploadImage(uri, path, userId, () =>
			this.AddImageToGroup(path, groupId)
		);
	}

	async UploadImage(uri, path, userId, callback) {
		const image = await fetch(uri);
		const blob = await image.blob();

		var metadata = {
			userId: userId,
		};

		firebase
			.storage()
			.ref()
			.child(path)
			.put(blob, metadata)
			.then((snapshot) => {
				console.log('Uploaded image!');

				callback();
			});
	}

	AddImageToGroup(path, groupId) {
		firebase
			.firestore()
			.collection('groups')
			.doc(groupId)
			.update({
				images: firebase.firestore.FieldValue.arrayUnion(path),
			})
			.then((docRef) => {
				console.log('Added image link to group');
			})
			.catch((error) => {
				console.error('Error adding image link to group: ', error);
			});
	}

	async FetchGroupImages(groupId, callback) {
		const imageRefs = await firebase
			.storage()
			.ref()
			.child(`groups/${groupId}`)
			.listAll();

		const urls = await Promise.all(
			imageRefs.items.map((ref) => ref.getDownloadURL())
		);

		callback(urls);
	}

	async FetchProfileImages(members, callback) {
		var urls = await members
			.filter((member) => member.photo !== null)
			.reduce(
				async (acc, member) => ({
					...(await acc),
					[member.id]: await firebase
						.storage()
						.ref(member.photo)
						.getDownloadURL(),
				}),
				{}
			);

		callback(urls);
	}

	async FetchImage(path, callback) {
		console.log('Fetching');

		firebase
			.storage()
			.ref(path)
			.getDownloadURL()
			.then((url) => {
				console.log('Image fetched from firebase');
				callback(url);
			})
			.catch((error) => {
				console.log(`Error fetching image: ${error}`);
				// A full list of error codes is available at
				// https://firebase.google.com/docs/storage/web/handle-errors
			});
	}
}

const firebaseAdapter = new FirebaseAdapter();

export default firebaseAdapter;
