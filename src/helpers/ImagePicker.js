import { Platform } from 'react-native';
import * as ImagePicker from 'expo-image-picker';

export default async function LaunchImagePicker(callback, aspect) {
	var status = await CheckPermission();

	if (status !== 'granted') {
		alert('Sorry, we need camera roll permissions to make this work!');
	} else {
		PickImage(callback, aspect);
	}
}

const PickImage = async (callback, aspect) => {
	let result = await ImagePicker.launchImageLibraryAsync({
		mediaTypes: ImagePicker.MediaTypeOptions.Images,
		allowsEditing: true,
		quality: 1,
		aspect: aspect,
	});

	if (!result.cancelled) {
		callback(result.uri);
	}
};

const CheckPermission = async () => {
	if (Platform.OS !== 'web') {
		const { status } = await ImagePicker.requestMediaLibraryPermissionsAsync();

		return status;
	}

	return 'Unexpected';
};
