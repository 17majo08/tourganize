// Currently main currency is always EUR.
export default function toMainCurrency(amount, currency) {
	return amount / getRate(currency);
}

const getRate = (currency) => {
	switch (currency) {
		case 'EUR':
			return 1.0;
		case 'CHF':
			return 1.0541254;
		case 'SEK':
			return 10.089965;
	}
};
